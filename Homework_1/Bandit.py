import numpy as np
from scipy.stats import bernoulli

class MultiArmedBandit:
    # create a multi-armed bandit with k arms
    def __init__(self, k: int):
        self.k = k
        self.T = np.zeros(k) # number of observations for each arm
        self.est_mean = np.zeros(k)
        self.opt_mean = max(self.means)
        self.regret = 0
        self.ucb = np.array(np.ones(self.k)* np.inf)

    def pull(self, arm):
        self.T[arm] += 1
        reward = self.get_reward(arm)
        self.update_est(arm, reward)
        self.update_regret(arm)
        return reward

    def update_est(self, arm, reward):
        self.est_mean[arm] += (reward - self.est_mean[arm])/self.T[arm]

    def update_regret(self, arm):
        self.regret += self.opt_mean - self.means[arm]

    def update_ucb(self, arm, delta):
        self.ucb[arm] = self.est_mean[arm] + np.sqrt(2 * np.log(1 / delta) / self.T[arm])

    def best_mean_action(self):
        return np.argmax(self.est_mean)

    def ucb_action(self):
        # returns the action with highest UCB
        return np.argmax(self.ucb)


class UniformBandit(MultiArmedBandit):
    def __init__(self, k: int, upper: np.array = np.array([]), lower: np.array = np.array([])):
        self.upper = np.ones(k)
        self.lower = np.zeros(k)
        if len(upper) == k:
            self.upper = upper
        if len(lower) == k:
            self.lower = lower
        self.means = self.lower + (self.upper - self.lower) / 2
        super().__init__(k)

    def get_reward(self, arm):
        return np.random.uniform(self.lower[arm], self.upper[arm])


class GaussianBandit(MultiArmedBandit):
    def __init__(self, k, means: np.array = np.array([])):
        self.means = np.random.rand(k)
        if len(means) == k:
            self.means = means
        super().__init__(k)

    def get_reward(self,arm):
        return np.random.normal(self.means[arm], 1) # + np.random.standard_normal() # random variables 1-subgaussian


class BernoulliBandit(MultiArmedBandit):
    def __init__(self, k, means: np.array = np.array([])):
        self.means = np.random.rand(k)
        if len(means) == k:
            self.means = means
        self.b = [bernoulli(means[i]) for i in range(0,k)]
        super().__init__(k)

    def get_reward(self,arm):
        return self.b[arm].rvs()