## this file is mainly used for first implementation and testing.
## for the implementation of the figures in the exercise see jupiter files.

import Bandit
import numpy as np
from matplotlib import pyplot as plt

def UCB(bandit):
    for t in range(0,n):
        action = bandit.ucb_action()
        bandit.pull(action)
        bandit.update_ucb(action,delta)

def ETC(bandit, m, k):
    for t in range(0, m*k):
        bandit.pull(t % k)
    a = bandit.best_mean_action()
    # print("best action is " + str(a) + ", with mean " + str(bandit.means[a]) + ", and estimated means : " + str(bandit.est_mean))
    for t in range(m*k, n):
        bandit.pull(a)
        # print("regret after action is " + str(bandit.regret) )

# input
# number of arms k and horizon n, delta = 1/n^2?
k = 2
n = 55
delta = 1/n**2

# Test UCB
ExpRegret_UCB = np.array([])
ExpRegret_ETC = np.array([])
#for Delta in np.arange(0.1, 1, 0.1):
Delta = 0.9
m = 50
means = [0,-Delta]
tot_regret_UCB = 0
tot_regret_ETC = 0
n_simulations = 10
for i in range(0, n_simulations):
    # bandit_UCB = Bandit.GaussianBandit(k, means)
    bandit_ETC = Bandit.GaussianBandit(k, means)
    # print("optimal mean: " + str(bandit_ETC.opt_mean))
    # UCB(bandit_UCB)
    ETC(bandit_ETC, m, k)
    # tot_regret_UCB += bandit_UCB.regret
    tot_regret_ETC += bandit_ETC.regret
# ExpRegret_UCB = np.append(ExpRegret_UCB, tot_regret_UCB/n_simulations)
ExpRegret_ETC = np.append(ExpRegret_ETC, tot_regret_ETC / n_simulations)



# plt.plot(np.arange(0.1, 1, 0.1),ExpRegret_UCB, label = 'UCB')
# plt.plot(np.arange(0.1, 1, 0.1),ExpRegret_ETC, label = 'ETC (m = 50)')
# plt.xlabel('Delta')
# plt.ylabel('Expected regret')
# plt.legend()
# plt.show()
