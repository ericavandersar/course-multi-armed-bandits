import Bandit
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# policy
def policy(bandit, k):
    for t in range(0, k):
        bandit.pull(t)
    for t in range(k, n):
        a = bandit.best_mean_action()
        bandit.pull(a)

# input
# number of arms k and horizon n
k = 2
n = 200

df = pd.DataFrame(columns=['l1','u1','l2','u2','regret'])
for upper1 in np.arange(0.1,1.1,0.1):
    for lower1 in np.arange(0,1,0.1):
        if lower1 < upper1:
            for upper2  in np.arange(0.1,1.1,0.1):
                for lower2 in np.arange(0,1,0.1):
                    if lower2 < upper2:
                        mean1 = lower1 + (upper1 - lower1) / 2
                        mean2 = lower2 + (upper2 - lower2) / 2
                        # Note: if mean1 == mean2 the regret will always be 0, so therefore skip these.
                        # due to rounding errors check if mean1 - mean2 > 0.0001
                        if (mean1 - mean2) > 0.0001:
                            upper = np.array([upper1, upper2])
                            lower = np.array([lower1, lower2])
                            total_regret = 0
                            # execute policy 10 times and use average regret in table
                            tryouts = 5
                            for i in range(0,tryouts):
                                # create bandits
                                uni_bandit = Bandit.UniformBandit(k, upper, lower)
                                # execute policy
                                policy(uni_bandit, k)
                                total_regret += uni_bandit.regret
                            regret = total_regret / tryouts
                            df = df.append({'l1': lower1,'u1': upper1,'l2': lower2, 'u2': upper2, 'regret': regret},ignore_index=True)
                    else:
                        break
        else:
            break

print(df)


### testing.
# # upper and lower bounds
# upper = np.array([0.5, 1])
# lower = np.array([0, 0])
# # create bandits
# start = time.time()
# uni_bandit = Bandit.UniformBandit(k,upper, lower)
# mid = time.time()
# policy(uni_bandit,k)
#
#
# # regret = np.array([0])
# # reward = np.array([0])
# # for t in range(0,k):
# #     arm = t
# #     reward = np.append(reward, uni_bandit.pull(arm))
# #     regret = np.append(regret, uni_bandit.regret)
# # for t in range(k,n):
# #     arm = uni_bandit.best_mean_action()
# #     reward = np.append(reward, uni_bandit.pull(arm))
# #     regret = np.append(regret, uni_bandit.regret)
#
# action = uni_bandit.best_mean_action()
# print(action)
# print(uni_bandit.est_mean)
# print(uni_bandit.means)
# print(uni_bandit.opt_mean)
# print(uni_bandit.regret)
# end = time.time()
# print("elapsed creating: " + str(mid-start) + " total elapsed time " + str(end - start))
# plt.plot(np.arange(0,n+1),reward, label='reward')
# plt.plot(np.arange(0,n+1),regret, label='regret')
# plt.legend()
# plt.show()
