from Homework_1 import Bandit
import numpy as np
from tqdm import tqdm
from matplotlib import pyplot as plt
from math import log

def UCB(bandit):
    action = bandit.ucb_action()
    bandit.pull(action)
    bandit.update_ucb(action,delta)

# input
# number of arms k and horizon n, delta = 1/n^2?
k = 2
n = 50_000
delta = 1/n**2

n_sim = 10
for Delta in tqdm(np.arange(0.1, 1.1, 0.1),desc="Running through delta's"):
    av_regret = np.zeros(n, dtype=np.float64)
    means = [Delta, 0]
    for sim in range(0, n_sim):
        regret = np.array([])
        bandit = Bandit.BernoulliBandit(k, means)
        for i in range(0, n):
            UCB(bandit)
            regret = np.append(regret, bandit.regret)
        av_regret += regret
    av_regret /= n_sim
    plt.plot(np.arange(0, n), av_regret, label="$\Delta$: %.1f" % Delta)

plt.xlabel('T')
plt.ylabel('regret')
plt.legend()
plt.title("Regret Bernoulli Bandits")
plt.savefig("Exercise_8_BernBand.png")
plt.show()



for c in [2**(2**x)  for x in np.arange(2.2, 1.25, -0.1)]: # np.arange(15, 2, -2): #
    plt.plot(np.arange(1, n), [c*log(x) for x in range(1, n)], label="c: %.2f" % c)

plt.xlabel('T')
plt.ylabel('y')
plt.legend()
plt.ylim([0,270])
plt.title("cLog(T)")

plt.savefig("Exercise_8_cLog(T).png")
plt.show()