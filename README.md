# Course Multi Armed Bandits

For programming exercises of the PhD course Multi-Armed Bandit Theory and its Applications

## Content

- Creating bandits with:
    - Uniform distribution
    - Gaussian distribution

- Policies
    - own policy (ETC with 1 exploration)
    - ETC
    - UCB

## Usage
```
pip install numpy 
pip install pandas 
pip install matplotlib
```
<!-- - [ ] some text -->

