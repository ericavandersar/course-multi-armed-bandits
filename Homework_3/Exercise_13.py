import numpy as np
from matplotlib import pyplot as plt
from PricingPolicies import Clairvoyant, CILS
import time

'''
What do we need?

1. Real Demand function (theta = (alpha,Beta) )
    D_t = alpha + Beta*p_t + epsilon_t          = theta_1 - theta_2 * p_t + epsilon_t
    epsilon ~ N(0,sigma)
2. Least square estimate function
    theta(t) = argmin_\theta ( sum_i=1^t D_i - alpha - Beta * p_i)^2
3. Greedy iterated least squares (ILS) policy   >   maximize p(alpha + Beta * p) :
    p_t+1 = \phi(theta_t) = \phi(alpha,Beta) = - alpha / 2*Beta
4. CILS policy
    set delta_t = \phi(theta_t) - average(p_t)
    if |delta_t| < kappa * t^{-1/4}
        p_t+1 = average(p_t) + sgn(delta_t) * kappa * t^{-1/4}
    else
        p_t+1 = \phi(theta_t)
'''

# define parameters
alpha = 1.1
beta = -0.5
sigma = 0.1
price_lower = 0.75
price_upper = 2
init_prices = np.array([1,2])
horizon = 100_000

clairvoyant = Clairvoyant(alpha, beta, sigma)

t = time.time_ns()
for kappa in np.arange(0, 1, 0.1):
    # print(kappa)
    my_policy = CILS(init_prices, price_upper, price_lower, kappa, clairvoyant=clairvoyant)
    my_policy.apply_policy(horizon)
    plt.plot(np.arange(0, horizon+init_prices.size), my_policy.regret, label="$\kappa$: %.1f" % kappa)
t = time.time_ns() - t

# regret_T1000 = []
# regret_T10000 = []
# regret_T20000 = []
# regret_T40000 = []
# regret_T60000 = []
# regret_T80000 = []
# regret_T100000 = []
#
# for kappa in np.arange(0, 1, 0.1):
#     # print(kappa)
#     my_policy = CILS(init_prices, price_upper, price_lower, kappa, clairvoyant=clairvoyant)
#     my_policy.apply_policy(horizon)
#     regret_T1000 += [my_policy.regret[1000]]
#     regret_T10000 += [my_policy.regret[10000]]
#     regret_T20000 += [my_policy.regret[20000]]
#     regret_T40000 += [my_policy.regret[40000]]
#     regret_T60000 += [my_policy.regret[60000]]
#     regret_T80000 += [my_policy.regret[80000]]
#     regret_T100000 += [my_policy.regret[10000]]


# plt.plot(np.arange(0, horizon+init_prices.size), my_policy.regret, label="$\kappa$: %.1f" % kappa)
#
print(t)

plt.xlabel('T')
plt.ylabel('regret')
plt.legend()
plt.title("Regret CILS")
plt.savefig("Exercise_13_Regret_CILS_kappavalues.png")
plt.show()

