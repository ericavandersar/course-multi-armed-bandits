import numpy as np
from tqdm import tqdm
from sklearn.linear_model import LinearRegression
from scipy.linalg import lstsq

'''
What do we need?

1. Real Demand function (theta = (alpha,Beta) ) > in clairvoyant
    D_t = alpha + Beta*p_t + epsilon_t          = theta_1 - theta_2 * p_t + epsilon_t
    epsilon ~ N(0,sigma)
2. Least square estimate function
    theta(t) = argmin_\theta ( sum_i=1^t D_i - alpha - Beta * p_i)^2
3. Greedy iterated least squares (ILS) policy   >   maximize p(alpha + Beta * p) :
    p_t+1 = \phi(theta_t) = \phi(alpha,Beta) = - alpha / 2*Beta
4. CILS policy
    set delta_t = \phi(theta_t) - average(p_t)
    if |delta_t| < kappa * t^{-1/4}
        p_t+1 = average(p_t) + sgn(delta_t) * kappa * t^{-1/4}
    else
        p_t+1 = \phi(theta_t)
5. Regret function
    compute revenue loss >> in clairvoyant
'''


# class to compute the real demand
class Clairvoyant:
    def __init__(self, alpha, beta, std_div):
        self.alpha = alpha
        self.beta = beta
        self.sigma = std_div
        self.opt_price = -self.alpha / (2 * self.beta)
        # optimal expected single period revenue:
        self.opt_revenue = self.opt_price * (self.alpha + self.beta * self.opt_price)

    # compute demand for time step t with price p_t
    # D_t = alpha + Beta * p_t + epsilon_t = theta_1 - theta_2 * p_t + epsilon_t
    def get_demand(self, price):
        return self.alpha + self.beta * price + np.random.normal(0, self.sigma)

    # regret / revenue loss for one time step
    def get_regret(self, price):
        return self.opt_revenue - price * (self.alpha + self.beta * price)

'''
Greedy iterated least squares (ILS) policy   >   maximize p(alpha + Beta * p) :
    p_t+1 = \phi(theta_t) = \phi(alpha,Beta) = - alpha / 2*Beta
'''
class ILS:
    def __init__(self, init_prices: np.array, price_up: float, price_low: float, clairvoyant: Clairvoyant = None):
        # self.theta = np.array([])
        self.alpha = 0
        self.beta = 0
        self.clairvoyant = clairvoyant
        self.price_up = price_up
        self.price_low = price_low
        self.prices = init_prices

        self.demands = self.clairvoyant.get_demand(self.prices)
        self.regret = np.array([self.clairvoyant.get_regret(self.prices[0])])
        for p in self.prices[1:]:
            self.update_regret(p)

        # update parameters
        self.least_squares_est()


    def least_squares_est(self):
        X = np.c_[np.ones(self.prices.size), self.prices]
        # theta = (X^T * X )^-1 * X^T * Y
        self.alpha, self.beta = np.linalg.inv(X.T @ X) @ X.T @ self.demands

        # reg = LinearRegression().fit(self.prices.reshape(-1,1), self.demands)
        # self.alpha = reg.intercept_
        # self.beta = reg.coef_[0]

        # X = np.c_[np.ones(self.prices.size), self.prices]
        # [self.alpha,self.beta], _, _, _ = lstsq(X, self.demands, lapack_driver='gelsy', check_finite=False)


    def apply_price(self, new_price: float):
        # update price vector
        self.prices = np.append(self.prices, new_price)
        # update demand vector
        self.demands = np.append(self.demands, self.clairvoyant.get_demand(new_price))
        # update lease squares estimate
        self.least_squares_est()
        # update regret
        self.update_regret(new_price)

    def update_regret(self, new_price: float):
        self.regret = np.append(self.regret, self.regret[-1] + self.clairvoyant.get_regret(new_price))


    # maximize p(alpha + Beta * p) :
    # p_t+1 = \phi(theta_t) = \phi(alpha,Beta) = - alpha / 2*Beta
    def policy(self):
        return -self.alpha / (2 * self.beta)

    def apply_policy(self, time_steps : int):
        for _ in tqdm(range(0,time_steps),desc="Applying policy for {0} time steps".format(time_steps)):
            new_price = self.policy()
            if new_price > self.price_up:
                print("New price estimate: {0} is too high!  >>> It needs to be within [{1},{2}]".format(new_price, self.price_up, self.price_low))
                new_price = self.price_up
            elif new_price < self.price_low:
                print("New price estimate: {0} is too low!  >>> It needs to be within [{1},{2}]".format(new_price, self.price_up, self.price_low))
                new_price = self.price_low
            self.apply_price(new_price)
            # print(self.prices)
            # print(self.regret)


'''
CILS policy
    set delta_t = \phi(theta_t) - average(p_t)
    if |delta_t| < kappa * t^{-1/4}
        p_t+1 = average(p_t) + sgn(delta_t) * kappa * t^{-1/4}
    else
        p_t+1 = \phi(theta_t)
'''
class CILS(ILS):
    def __init__(self, init_prices: np.array, price_up: float, price_low: float, kappa: float, clairvoyant: Clairvoyant = None):
        self.kappa = kappa
        super().__init__(init_prices, price_up, price_low, clairvoyant=clairvoyant)

    def policy(self):
        ILS_price = super().policy()
        delta = ILS_price - np.average(self.prices)
        if np.abs(delta) < self.kappa * self.prices.size ** (-1/4):
            # print("deviate from greedy")
            return np.average(self.prices) + np.sign(delta) * self.kappa * self.prices.size ** (-1/4)
        else:
            return ILS_price



